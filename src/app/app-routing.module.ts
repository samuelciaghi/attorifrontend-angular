import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisualizzaAttoriComponent } from './visualizza-attori/visualizza-attori.component';

const routes: Routes = [
 {path: '', component: VisualizzaAttoriComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
