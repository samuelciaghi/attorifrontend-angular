import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Attori } from './Attori';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class FetchSpringService {

  constructor(private http: HttpClient) { }

  riceviAttori(): Observable<Attori[]> {

    return this.http.get<Attori[]>('http://localhost:3001')
    .pipe(map((attoriRicevuti: any) => attoriRicevuti.sort((a, b) => 
    {
        if (a.nomeAlternativo === "None") a.nomeAlternativo = "-"
        return b.rating - a.rating
      })))
    }

}
