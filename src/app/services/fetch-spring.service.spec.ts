import { TestBed } from '@angular/core/testing';

import { FetchSpringService } from './fetch-spring.service';

describe('FetchSpringService', () => {
  let service: FetchSpringService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FetchSpringService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
