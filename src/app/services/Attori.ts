
export interface Attori{
    id:number,
    nome:string,
    nomeAlternativo?:string,
    rating:number
}