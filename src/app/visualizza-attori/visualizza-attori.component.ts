import { Component, OnInit } from '@angular/core';
import { Attori } from '../services/Attori';
import { FetchSpringService } from '../services/fetch-spring.service';

@Component({
  selector: 'app-visualizza-attori',
  templateUrl: './visualizza-attori.component.html',
  styleUrls: ['./visualizza-attori.component.css']
})
export class VisualizzaAttoriComponent implements OnInit {

  attoriRicevuti: Attori[];
  varTemp: Attori[]
  ordinato: boolean = true;
  scelta: string;

  constructor(private fetchService: FetchSpringService) { }

  ngOnInit(): void {

    this.getAttori()

  }

  getAttori() {

    this.fetchService.riceviAttori()
      .subscribe(datiSpring => {
        this.attoriRicevuti = datiSpring;
        console.log(this.attoriRicevuti)
      }).add(() => this.varTemp = this.attoriRicevuti)

  }

  ordinaRating() {

    this.attoriRicevuti.sort((a, b) => {
      if (this.ordinato == true) { return a.rating - b.rating; }
      else { return b.rating - a.rating; }
    });

    this.ordinato = !this.ordinato;
  }

  mostraMeno() {
    if (this.scelta === undefined) this.scelta = '20'
    let soloNumeri = /[a-zA-Z]+/g
    let temp = this.varTemp

    this.attoriRicevuti = [...temp].splice(0, +this.scelta);


    if (+this.scelta <= 0 || +this.scelta == undefined || this.scelta == '' || this.scelta.match(soloNumeri)) {
      this.attoriRicevuti = this.varTemp;
    }
  }

}
