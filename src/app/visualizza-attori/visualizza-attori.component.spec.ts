import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaAttoriComponent } from './visualizza-attori.component';

describe('VisualizzaAttoriComponent', () => {
  let component: VisualizzaAttoriComponent;
  let fixture: ComponentFixture<VisualizzaAttoriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizzaAttoriComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaAttoriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
